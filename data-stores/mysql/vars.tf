variable "db_password" {
  description = "The password for the database"
}

variable "instance_class" {
  description = "The class of RDS Instance to run (e.g. db.t2.micro)"
}

variable "allocated_storage" {
  description = "The amount of GB of storage to allocate to the RDS Instance"
}
