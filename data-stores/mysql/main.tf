resource "aws_db_instance" "example" {
  engine              = "mysql"
  allocated_storage   = "${var.allocated_storage}"
  instance_class      = "${var.instance_class}"
  name                = "example_database"
  username            = "admin"
  password            = "${var.db_password}"
  skip_final_snapshot = true
}
